# Overview

This is the **gfa-status-panels** git repository which provides a set of overview status panels for GFA's
main scientific facilities.
 
The project is an NPM project which uses the rollup.js package to create a single JS support files to support
the html file for each scientific facility.

# How to build

```
  npm run build
```

# How to deploy

Copy contents of the build directory created in the step above to one or all of 'dist_dev'/'dist_prod'/'dist_ext' to 
deploy on the wica dev/prod/external wica servers. 
```
  npm run dist_publish_all
```

# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.
