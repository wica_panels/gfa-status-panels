import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";

export default [ {
       input:  'src/proscan/proscan-support.js',
       output: {
           dir: 'build',
           format: 'esm',
           sourcemap: true,
       },
       plugins: [
           resolve(),  // so Rollup can find modules.
           commonjs(), // so Rollup can convert the input to an ES module.
           copy( {
               targets: [
                   { src: "src/proscan/comet-cryo.html", dest: "build" },
                   { src: "src/proscan/comet.html", dest: "build" },
                   { src: "src/proscan/proscan.html", dest: "build" },
                   { src: "src/proscan/proscan-debug.html", dest: "build" }
               ],
           }),
       ]
    },
    {
        input:  'src/hipa/hipa-support.js',
        output: {
            dir: 'build',
            format: 'esm',
            sourcemap: true,
        },
        plugins: [
            resolve(),  // so Rollup can find modules.
            commonjs(), // so Rollup can convert the input to an ES module.
            copy( {
                targets: [
                    { src: "src/hipa/hipa.html", dest: "build" },
                    { src: "src/hipa/hipa-responsive.html", dest: "build" }
                ],
            }),
        ]
    }
];