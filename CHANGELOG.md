# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  
* [1.0.0-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.0.0)
  Initial release created from old wica_panels project.
  
* [1.1.0-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.1.0)
  * PROSCAN: Added prototype support for proscan chat.

* [1.2.0-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.2.0)
  * PROSCAN: Added support for responsive design ie basic functionality on mobile devices and tablets.
  * PROSCAN: Added support for display of Operator Messages.         
  
* [1.3.0-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.0) 2020-01-22
  * PROSCAN: Adapted COMET to use plot buffer.
  * Create epics directory with working template ans subs file to support plot buffering.    

* [1.3.1-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.1) 2020-01-25
  * PROSCAN: Fixed responsive display issues when PS1 and PS2 driven out.
  * PROSCAN: Made responsive display the version that is published at the standard 'xxx/gfa/status/proscan.html' URL.
  * PROSCAN: Added JS redirection from old 'xxx/gfa/status/proscan-responsive.html' URL.

* [1.3.2-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.2) 2020-02-25
  PROSCAN: increased precision of Hauptmagnet to 4 digits following request from Bunker Crew.
  
* [1.3.3-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.3) 2020-09-16
  * PROSCAN: Fixed bug whereby precision of Hauptmagnet data channel was still only 3 digits.
  * PROSCAN: Improved null handling in updateChatAcknowledgementWidget to eliminate error messages.
  * PROSCAN: Removed support for proscan-classic.
  * PROSCAN: Added support for proscan-debug page to test new ica attributes 'data-wica-stream-name' and 'data-wica-stream-props'.
  * GENERAL: Removed unnecessary badge created by earlier cut-and-paste error.
  * GENERAL: Upgraded dependencies to latest.
  * GENERAL: Updated CHANGELOG with details of 1.3.3-RELEASE.
  * GENERAL: Suppress warnings about missing 'wica.js' and 'modernizr.js' libraries. These will be colocated on deployment server.

* [1.3.4-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.4) 2020-09-19
  * COMET:   Fixed formatting of vacuum values to 1 digit precision. 
  * PROSCAN: Changed calculators to update 'data-wica-stream-state' attribute in the correct format so that 
             the element can be colourised appropriately via the wica.css stylesheet.
  * GENERAL: Added support for extracting the connection attempt number from the data-wica-stream-state attribute.
  * GENERAL: Removed modernizr as this is now provided directly from the node modules tree.
  * GENERAL: Updated CHANGELOG with details of 1.3.4-RELEASE.

* [1.3.5-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.5) 2021-01-20
  * PROSCAN: Changed Phase variable from: MMJF:IST:2 to MMJF:H3:PHASE:2, following email 
    request by Hubert Lutz on 2020-01-20.

* [1.3.6-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.6) 2021-02-04
  * PROSCAN: Changed scaling of PROSCAN Vdef upper limit to 2.6kV following discussion with Zema Chowdhuri.

* [1.3.7-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.7) 2021-02-12
  * PROSCAN: Changed Phase variable from: MMJF:H3:PHASE:2 MMJF:H0:PHASE:2, following email
    request by Jochem Snuverlink on 2021-02-11.

* [1.3.8-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.3.8) 2021-03-29
  * PROSCAN: Added COMET Phase Status Widget following request from Zema Chowdhuri.
  
* [1.4.0-RELEASE](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.4.0) 2021-06-24
  * CRYO GROUP: Added COMET Shield Head Monitoring Plot following request from Martin Meuwly.
  * GENERAL: Improved comments.
  * GENERAL: Fixed lint errors
  * GENERAL: Now builds with wica-js node module. Retired local copy of source code.
  * GENERAL: Upgraded to 1.4.0 and now uses renamed build_script.sh
  * GENERAL: Upgraded Plotly library to latest 2.1.0 (now incorporates bug fix which eliminates 
             need to patch node_modules area)

* [1.4.1](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/1.4.1) 2021-06-24
  * GENERAL: BUG FIX: Zoom2D button has been re-enabled on plots as this is needed to allow
    users to restore normal behaviour after switching to pan mode.

* [2.0.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.0.0) 2021-07-05
  * GENERAL: The project has been completely refactored to eliminate the use of shared templates.
  * HIPA: The status display now uses plot data derviced from a SoftIOC. This means the 
    back-history of the facility beam current and target losses is always available.
  * HIPA: the project now includes a prototype status display that can be viewed on a mobile.
    This will be installed at the endpoint: https://wica.psi.ch/gfa/status/hipa-responsive.html

* [2.1.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.1.0) 2021-10-21
  * PROSCAN: Removed chat acknowledgment checkboxes on request from Sima. See [tracker](https://codebeamer.psi.ch/cb/issue/174337)

* [2.2.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.2.0) 2022-08-30
  * PROSCAN COMET: Address issues raised by Richard Kan in email of 2022-08-30.

* [2.3.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.3.0) 2022-08-30
  * HIPA: Fixed broken beam loss channel names 2022-11-21

* [2.4.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.4.0) 2023-08-17
  * HIPA: Adapt for HUSH application.

* [2.5.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.5.0) 2023-10-16
  * PROSCAN: Added phase regulation SOL value.

* [2.6.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.6.0) 2023-10-31
  * PROSCAN: Added phase regulation dynamic IST colourisation.

* [2.7.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.7.0) 2023-11-17
  * PROSCAN: Following request from Zema Chowdhuri made the changes below.
     * Added comet curve to not show 6 decimal digits. 
     * Add 2 decimal digits to current IST and SOL. 

* [2.8.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.8.0) 2024-04-05
  * PROSCAN COMET: Following request from Hubert Lutz.
  * ENHANCEMENT: Update 'YMJHH:SPA:2' to 'YMJHH:SPARB:2' following Hubert's JIRA request 'PROSCAN-52'.

* [2.9.0](https://gitlab.psi.ch/wica_panels/gfa-status-panels/tags/2.9.0) 2024-10-16
  * BUG FIX: Change Y-Axis units on beam losses plot from uA to nA (request via Christian Baumgarten).
  * BUG FIX: Change channel names for beam loss monitor plot from MRI12-xx to MRI2-xx.
  * ENHANCEMENT: Upgrade precision of beam loss monitors to 1 decimal digit.
  * Create release 2.9.0.
