console.debug( "Executing script in hipa-beam-losses-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class HipaBeamLossesPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle, yanchor: "middle" },
            titlefont: {color: "darkblue"},
            legend: {x: 0.2, y: -0.125, font: { size: 9 }, orientation: "h", yanchor: "top" },
            margin: {l: 60, r: 60, b: 60, t: 40 },
            dragmode: true,
            xaxis: {
                type: "date",
                ticklen: 5,
                autorange: true
            },
            yaxis: {
                title: { text: "MRI2, MII7 - nA" },
                range: [0, 30],
                tickvals: [0, 10, 20, 30],
                scaleratio: 1,
                tickfont: {color: "black"}
            },
            yaxis2: {
                range: [0, 30],
                tickvals: [5, 15, 25],
                overlaying: "y",
                anchor: "x",
                side: "left",
                scaleratio: 1,
                tickfont: {color: "red"}
            },
            yaxis3: {
                title: {text: "MRI13, MRI14 - nA"},
                range: [0, 900],
                tickvals: [0, 300, 600, 900],
                overlaying: "y",
                anchor: "x",
                side: "right",
                scaleratio: 1,
                tickfont: {color: "blue"}
            },
            yaxis4: {
                range: [0, 900],
                tickvals: [150, 450, 750 ],
                overlaying: "y",
                anchor: "x",
                side: "right",
                scaleratio: 1,
                tickfont: {color: "limegreen"}
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        const mii7Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MII7",
            yaxis: "y",
            marker: {size: 2},
            line: {width: 1, color: "black"},
            fillcolor: "rgba( 144, 238, 144, 0.1)", // reduce opacity of "lightgreen" to show gridlines
        };

        const mri2Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MRI2",
            yaxis: "y2",
            marker: {size: 2},
            line: {width: 1, color: "red"},
        };

        const mri13Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MRI13",
            yaxis: "y3",
            marker: {size: 2},
            line: {width: 1, color: "blue"},
        };

        const mri14Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MRI14",
            yaxis: "y4",
            marker: {size: 2},
            line: {width: 1, color: "limegreen"},
        };
        return [ mri2Trace, mii7Trace, mri13Trace, mri14Trace ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }

}