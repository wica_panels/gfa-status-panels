console.debug( "Executing script in hipa-target-current-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class HipaTargetCurrentPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle, yanchor: "middle" },
            titlefont: {color: "darkblue"},
            legend: {x: 0.2, y: -0.125, font: { size: 9 }, orientation: "h", yanchor: "top" },
            margin: {l: 60, r: 60, b: 60, t: 40 },
            dragmode: true,
            xaxis: {
                type: "date",
                ticklen: 5,
                autorange: true
            },
            yaxis: {
                title: {text: "MHC1, MBC1 - µA"},
                tickfont: {color: "limegreen"},
                range: [-10, 2000],
                tickvals: [500, 1500],
                scaleratio: 1
            },
            yaxis2: {
                tickfont: {color: "red"},
                range: [-10, 4000],
                tickvals: [0, 2000, 4000],
                overlaying: "y",
                anchor: "x",
                side: "left",
                scaleratio: 1
            },
            yaxis3: {
                title: {text: "MYC2 - µA" },
                tickfont: {color: "blue"},
                range: [0, 100],
                tickvals: [0, 25, 50, 75, 100],
                overlaying: "y",
                anchor: "x",
                side: "right",
                scaleratio: 1
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        const mhc1Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MHC1",
            yaxis: "y",
            marker: {size: 2},
            line: {width: 1, color: "limegreen"},
            fillcolor: "rgba( 144, 238, 144, 0.5)", // reduce opacity of "lightgreen" to show gridlines
            fill: "tozeroy"
        };

        const mbc1Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MBC1",
            yaxis: "y2",
            marker: {size: 2},
            line: {width: 1, color: "red"},
        };

        const myc2Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MYC2",
            yaxis: "y3",
            marker: {size: 2},
            line: {width: 1, color: "blue"},
            zeroline: false
        };

        return [ mhc1Trace, mbc1Trace, myc2Trace ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}
