console.debug( "Executing script in vertical-deflector-calc.js module...");

import WicaUtils from '../utils/wica-utils.js';
import JSON5 from 'json5';

const REFRESH_RATE_IN_MILLIS = 500;
const FIXED_PRECISION = 2;
const UNITS = "kV";

export class CalculateVerticalDeflector
{
    constructor( calcDivId, vdefPhaseDataId, vdefAmplitudeDataId )
    {
        this.calculationOutputElement = document.getElementById( calcDivId );
        this.vdefPhaseDataElement = document.getElementById( vdefPhaseDataId );
        this.vdefAmplitudeDataElement = document.getElementById( vdefAmplitudeDataId );

        // Update the tooltip to show the channels that contribute to the calculation
        const vdefPhaseChannelName = this.vdefPhaseDataElement.getAttribute( "data-wica-channel-name" );
        const vdefAmplitudeChannelName = this.vdefAmplitudeDataElement.getAttribute( "data-wica-channel-name" );
        const tooltip = vdefPhaseChannelName + ", " + vdefAmplitudeChannelName;
        this.calculationOutputElement.setAttribute( "data-wica-tooltip", tooltip );
    }

    activate()
    {
        this.startUpdateCycle();
    }

    startUpdateCycle()
    {
        this.updateHtmlElement();

        // Reschedule next update
        setTimeout( () => this.startUpdateCycle(), REFRESH_RATE_IN_MILLIS );
    }

    updateHtmlElement()
    {
        // Bail out if any of the Wica-aware elements are not connected to the Wica Server.
        const attemptNumber = WicaUtils.getStreamStateAttemptNumber( this.vdefPhaseDataElement );
        if ( WicaUtils.checkStreamStateConnecting( this.vdefPhaseDataElement, this.vdefAmplitudeDataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-stream-state", "connect-" + attemptNumber );
            return;
        }
        else if ( WicaUtils.checkStreamStateClosed( this.vdefPhaseDataElement, this.vdefAmplitudeDataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-stream-state", "closed-" + attemptNumber );
            return;
        }
        this.calculationOutputElement.setAttribute( "data-wica-stream-state", "opened-" + attemptNumber );

        // Bail out if any of the Wica-aware elements indicate that the Wica Server is not connected to the
        // underlying data channel.
        if ( WicaUtils.checkChannelStateDisconnected( this.vdefPhaseDataElement, this.vdefAmplitudeDataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }
        this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "connected" );

        // Attempt to obtain the Wica channel's value objects.
        const vdefPhaseValueObj = JSON5.parse( this.vdefPhaseDataElement.getAttribute( "data-wica-channel-value-latest" ) );
        const vdefAmplitudeValueObj = JSON5.parse( this.vdefAmplitudeDataElement.getAttribute( "data-wica-channel-value-latest" ) );

        // Bail out if any of the Wica channels are offline.
        if ( ! WicaUtils.checkRemoteDataSourceOnline( vdefPhaseValueObj, vdefAmplitudeValueObj ) ) {
            this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }

        // This calculation was taken from the PROSCAN caQtDM display's visibility calculator
        // Updated 2021-06-29 following reexamination of polarity indicator test in 'proscan-status.ui' file.
        const vdefPhaseIndicator = ( vdefPhaseValueObj.val === 0x41 ) || ( vdefPhaseValueObj.val === 0x42 ) ? "-" :
                                   ( vdefPhaseValueObj.val === 0x21 ) || ( vdefPhaseValueObj.val === 0x22 ) ? "+" : "?";

        // If we get here update the target element to show the current value
        this.calculationOutputElement.firstChild.data = vdefPhaseIndicator + ( vdefAmplitudeValueObj.val ).toFixed( FIXED_PRECISION ) + " " + UNITS;
     }

}
