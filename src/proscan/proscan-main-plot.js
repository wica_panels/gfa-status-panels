console.debug( "Executing script in proscan-main-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class ProscanMainPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: { color: "darkblue" },
            legend: {x: 0.8, y: 1.2, font: {size: 11} },
            margin: {l: 80, r: 20, b: 40, t: 30},
            showlegend: false,
            dragmode: true,
            xaxis: {
                type: "date",
                autorange: true
            },
            yaxis: {
                title: {text: "MMAC3 (nA)", font: {color: "black"} },
                tickfont: {color: "black"},
                range: [0, 850],
                tick0: 0, dtick: 100,
                scaleratio: 1
            },
            yaxis2: {
                range: [0, 2.6], // Expected Range is 0 to 2.6kV
                overlaying: "y",
                anchor: "x",
                side: "left",
                scaleratio: 1,
                zeroline: false,
                showgrid: false,
                showticklabels: false,
            },
            yaxis3: {
                range: [0, 130], // Expected Range is 0 to 130kW
                overlaying: "y",
                anchor: "x",
                side: "right",
                scaleratio: 1,
                zeroline: false,
                showgrid: false,
                showticklabels: false,
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        };
    }

    getTraces()
    {
        const mmac3Trace = {
            type: "scatter",
            mode: "lines+markers",
            name: "MMAC3",
            yaxis: "y",
            marker: {size: 1},
            line: {width: 1, color: "black"},
            fillcolor: "limegreen",
            fill: "tozeroy"
        };

        const vdefTrace = {
            type: "scatter",
            mode: "lines+markers",
            name: "Deflector (rel)",
            yaxis: "y2",
            marker: {size: 1},
            line: {width: 1, color: "blue"},
        };

        const hfTrace = {
            type: "scatter",
            mode: "lines+markers",
            name: "HF (rel)",
            yaxis: "y3",
            marker: {size: 2},
            line: {width: 1, color: "black"},
        };

        return [ mmac3Trace, vdefTrace, hfTrace ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}
