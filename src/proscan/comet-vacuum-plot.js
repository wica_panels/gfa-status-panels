console.debug( "Executing script in comet-vacuum-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class CometVacuumPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: {color: "black" },
            legend: {x: 10, y: 1.2 },
            margin: {l: 75, r: 20, b: 40, t: 30 },
            showlegend: true,
            dragmode: true,
            yaxis: {
                type: "log",
                title: {text: "Vaku. Druck (mBar)", font: { color: "black" } },
                range: [ -9, -5 ],
                exponentformat: "e",
                hoverformat: "0.1e"
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        return [ {
            name: "SK Hoch",
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "darkblue" },
        },
        {
            name: "VK Hoch",
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "red" },
        } ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}