console.debug( "Executing script in heizleistung-calc.js module...");

import WicaUtils from '../utils/wica-utils.js';
import JSON5 from 'json5';

const REFRESH_RATE_IN_MILLIS = 1000;
const FIXED_PRECISION = 2;
const UNITS = "W";

export class CalculateHeizLeistung
{

    constructor( calcDivId, channel1DataId, channel2DataId )
    {
        this.calculationOutputElement = document.getElementById( calcDivId );
        this.channel1DataElement = document.getElementById( channel1DataId );
        this.channel2DataElement = document.getElementById( channel2DataId );

        // Update the tooltip to show the channels that contribute to the calculation
        let channel1Name = this.channel1DataElement.getAttribute( "data-wica-channel-name" );
        let channel2Name = this.channel2DataElement.getAttribute( "data-wica-channel-name" );
        let tooltip = channel1Name + ", " + channel2Name;
        this.calculationOutputElement.setAttribute( "data-wica-tooltip", tooltip );
    }

    activate()
    {
        this.startUpdateCycle();
    }

    startUpdateCycle()
    {
        this.updateHtmlElement();

        // Reschedule next update
        setTimeout( () => this.startUpdateCycle(), REFRESH_RATE_IN_MILLIS );
    }

    updateHtmlElement()
    {
        // Bail out if any of the Wica-aware elements are not connected to the Wica Server.
        const attemptNumber = WicaUtils.getStreamStateAttemptNumber( this.channel1DataElement );
        if ( WicaUtils.checkStreamStateConnecting( this.channel1DataElement, this.channel2DataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-stream-state", "connect-" + attemptNumber );
            return;
        }
        else if ( WicaUtils.checkStreamStateClosed( this.channel1DataElement, this.channel2DataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-stream-state", "closed-" + attemptNumber );
            return;
        }
        this.calculationOutputElement.setAttribute( "data-wica-stream-state", "opened-" + attemptNumber );

        // Bail out if any of the Wica-aware elements indicate that the Wica Server is not connected to the
        // underlying data channel.
        if ( WicaUtils.checkChannelStateDisconnected( this.channel1DataElement, this.channel2DataElement ) )
        {
            this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }
        this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "connected" );

        // Attempt to obtain the Wica channel's value objects.
        let channel1ValueObj = JSON5.parse( this.channel1DataElement.getAttribute( "data-wica-channel-value-latest" ) );
        let channel2ValueObj = JSON5.parse( this.channel2DataElement.getAttribute( "data-wica-channel-value-latest" ) );

        // Bail out if any of the Wica channels are offline.
        if ( ! WicaUtils.checkRemoteDataSourceOnline( channel1ValueObj, channel2ValueObj ) ) {
            this.calculationOutputElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }

        // If we get here update the target element to show the current value
        this.calculationOutputElement.firstChild.data = ( channel1ValueObj.val * channel2ValueObj.val ).toFixed( FIXED_PRECISION ) + " " + UNITS;
     }

}
