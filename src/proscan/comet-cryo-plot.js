console.debug( "Executing script in comet-cryo-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class CometCryoPlot
{
    constructor( plotTitle, traceNames )
    {
        this.plotTitle = plotTitle;
        this.traceNames = traceNames;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: {color: "darkblue" },
            legend: {x: 10, y: 1.2, font: {size: 11}},
            margin: {l: 75, r: 20, b: 40, t: 40 },
            showlegend: true,
            dragmode: true,
            yaxis: {
                title: {text: "Temperature (K)", font: { color: "black" } },
                range: [ 25, 105 ],
                tick0: 30,
                dtick: 10,
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        return [ {
            name: this.traceNames[ 0 ],
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "darkblue" },
        },
        {
            name: this.traceNames[ 1 ],
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "red" },
        },
        {
            name: this.traceNames[ 2 ],
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "green" },
        },
        {
            name: this.traceNames[ 3 ],
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "magenta" },
        },
        {
            name: this.traceNames[ 4 ],
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "yellow" },
        } ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}