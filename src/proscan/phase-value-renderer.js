console.debug( "Executing script in phase-value-renderer.js module...");

import WicaUtils from '../utils/wica-utils.js';
import JSON5 from 'json5';

const REFRESH_RATE_IN_MILLIS = 500;
const FIXED_PRECISION = 2;
const UNITS = "deg";

export class PhaseValueRenderer
{
    constructor( phaseSolValueDivId, phaseIstValueDivId, phaseLowerDifferenceLimitDivId, phaseUpperDifferenceLimitDivId )
    {
        this.phaseSolValueDataElement = document.getElementById( phaseSolValueDivId );
        this.phaseIstValueDataElement = document.getElementById( phaseIstValueDivId );
        this.phaseLowerDifferenceLimitDataElement = document.getElementById( phaseLowerDifferenceLimitDivId );
        this.phaseUpperDifferenceLimitDataElement = document.getElementById( phaseUpperDifferenceLimitDivId );

        // Update the tooltip to show the channels that contribute to the difference limit calculation
        this.phaseIstValueChannelName = this.phaseIstValueDataElement.getAttribute( "data-wica-channel-name" );
    }

    activate()
    {
        this.startUpdateCycle();
    }

    startUpdateCycle()
    {
        this.updateHtmlElement();

        // Reschedule next update
        setTimeout( () => this.startUpdateCycle(), REFRESH_RATE_IN_MILLIS );
    }

    updateHtmlElement()
    {
        // Bail out if any of the Wica-aware elements are not connected to the Wica Server.
        const attemptNumber = WicaUtils.getStreamStateAttemptNumber( this.phaseSolValueDataElement );
        if ( WicaUtils.checkStreamStateConnecting( this.phaseSolValueDataElement, this.phaseIstValueDataElement,
            this.phaseUpperDifferenceLimitDataElement, this.phaseLowerDifferenceLimitDataElement ) )
        {
            this.phaseIstValueDataElement.setAttribute( "data-wica-stream-state", "connect-" + attemptNumber );
            return;
        }

        else if ( WicaUtils.checkStreamStateClosed( this.phaseSolValueDataElement, this.phaseIstValueDataElement,
            this.phaseUpperDifferenceLimitDataElement, this.phaseLowerDifferenceLimitDataElement ) )
        {
            this.phaseIstValueDataElement.setAttribute( "data-wica-stream-state", "closed-" + attemptNumber );
            return;
        }
        this.phaseIstValueDataElement.setAttribute( "data-wica-stream-state", "opened-" + attemptNumber );

        // Bail out if any of the Wica-aware elements indicate that the Wica Server is not connected to the
        // underlying data channel.
        if ( WicaUtils.checkChannelStateDisconnected( this.phaseSolValueDataElement, this.phaseIstValueDataElement,
            this.phaseUpperDifferenceLimitDataElement, this.phaseLowerDifferenceLimitDataElement ) )
        {
            this.phaseIstValueDataElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }
        this.phaseIstValueDataElement.setAttribute( "data-wica-channel-connection-state", "connected" );

        // Attempt to obtain the Wica channel value objects.
        let phaseSolValueObj = JSON5.parse( this.phaseSolValueDataElement.getAttribute( "data-wica-channel-value-latest" ) );
        let phaseIstValueObj = JSON5.parse( this.phaseIstValueDataElement.getAttribute( "data-wica-channel-value-latest" ) );
        let phaseLowerDifferenceLimitObj = JSON5.parse( this.phaseLowerDifferenceLimitDataElement.getAttribute( "data-wica-channel-value-latest" ) );
        let phaseUpperDifferenceLimitObj = JSON5.parse( this.phaseUpperDifferenceLimitDataElement.getAttribute( "data-wica-channel-value-latest" ) );

        // Bail out if any of the Wica channels are offline.
        if ( ! WicaUtils.checkRemoteDataSourceOnline( phaseSolValueObj, phaseIstValueObj, phaseLowerDifferenceLimitObj, phaseUpperDifferenceLimitObj ) ) {
            this.phaseIstValueDataElement.setAttribute( "data-wica-channel-connection-state", "disconnected" );
            return;
        }

        // Calculate the upper and lower limits for the phase value
        let istValue = phaseIstValueObj.val;
        let lowerLimit = ( phaseSolValueObj.val - phaseLowerDifferenceLimitObj.val );
        let upperLimit = ( phaseSolValueObj.val + phaseUpperDifferenceLimitObj.val );
        let alarmState = ( istValue < lowerLimit ) || ( istValue > upperLimit ) ? "1" : "0";

        // If we get here update the target element to show the current value
        this.phaseIstValueDataElement.firstChild.data = istValue.toFixed(  FIXED_PRECISION) + " " + UNITS;
        this.phaseIstValueDataElement.dataset.wicaChannelPhaseValueAlarmState = alarmState;

        let tooltip = this.phaseIstValueChannelName + " (IST); with Dynamic Limits: [" + lowerLimit.toFixed(  FIXED_PRECISION) + " to " + upperLimit.toFixed( FIXED_PRECISION)  + "]";
        this.phaseIstValueDataElement.setAttribute( "data-wica-tooltip", tooltip );
     }
}
