console.debug( "Executing script in comet-sump-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class CometSumpPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: {color: "black" },
            legend: {x: 10, y: 1.2 },
            margin: {l: 75, r: 20, b: 40, t: 30 },
            showlegend: true,
            dragmode: true,
            yaxis: {
                type: "log",
                title: {text: "Temp. (K)" },
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        return [ {
            name: "SK Temp",
            type: "scatter",
            mode: "lines+markers",
            marker: { size: 3 },
            line: { width: 1, color: "darkblue" },
        } ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}