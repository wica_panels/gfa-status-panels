console.debug( "Executing script in comet-curve-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class CometCurvePlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: {color: "darkblue" },
            legend: {x: 0.8, y: 1.2, font: {size: 11}},
            margin: {l: 60, r: 30, b: 40, t: 30 },
            showlegend: false,
            dragmode: true,
            xaxis: {
                title: { text: "Vdef (kV)" },
                tickformat: "0.3f",
                tickwidth: 2
            },
            yaxis: {
                title: {text: "MMAC3 (nA)", font: {color: "black"} },
                tickformat: "0.0f"
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        return [ {
                name: "Learning Curve",
                type: "scatter",
                mode: "lines+markers",
                yaxis: "y",
                marker: { size: 4, symbol: "circle" },
                line: { width: 2 },
            },
            {
                name: "Regul. Point",
                type: "scatter",
                mode: "markers",
                yaxis: "y",
                marker: { size: 10, symbol: "cross" },
                line: { width: 2 },
            } ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}