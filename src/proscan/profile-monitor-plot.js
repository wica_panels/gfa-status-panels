console.debug( "Executing script in profile-monitor-plot.js module...");

import * as XyPlotConfig from "../plotsupport/xy-plot-config.js"

export class ProfileMonitorPlot
{
    constructor( plotTitle )
    {
        this.plotTitle = plotTitle;
    }

    getLayout()
    {
        return {
            title: { text: this.plotTitle },
            titlefont: {color: "darkblue" },
            legend: {x: 0.8, y: 1.2, font: {size: 11}},
            margin: {l: 40, r: 10, b: 60, t: 30},
            showlegend: false,
            dragmode: true,
            xaxis: {
                tick0: 0,
                dtick: 10,
                tickwidth: 2,
                range: [-32, 32]
            },
            paper_bgcolor: "rgba(0,0,0,0)", // Set opacity set to zero so that HTML background comes through.
            plot_bgcolor: "rgba(0,0,0,0)"
        }
    }

    getTraces()
    {
        return [ {
            type: "scatter",
            mode: "lines+markers",
            yaxis: "y",
            marker: {size: 2},
            // line: { width: 1, color: "black" },
            // fillcolor: "limegreen",
            fill: "tozeroy"
        } ];
    }

    getConfig()
    {
        return XyPlotConfig.xy_plot_config();
    }
}