console.debug( "Executing script in xy-date-plot.js module...");

import Plotly from "plotly.js-basic-dist";
import {PlotBuffer} from "@psi/wica-js/client-api.js";
import WicaUtils from "../utils/wica-utils.js";

export class XyPlotDirect
{
    /**
     * Creates a new plot for visualizing XY data.
     *
     * @param {Object} plotOutputId - The HTML element where the plot is to be rendered.
     *
     * @param {String[]} htmlElementIds - The elements providing the datasources for this plot.
     *
     * @param {Object[]} traces - An array with each entry providing a Plotly trace object. The
     *    EVEN indexed traces (0,2,4...) provide the X data; the ODD indexed traces provide the
     *    Y data.
     * @param {Object} layoutOptions - provides the Plotly plot layout object.
     * @param {Object} configOptions - provides the Plotly plot layout configuration options object.
     *
     * Note: the number of elements in the htmlElementIds array should exactly match the number
     *       of traces in the plot and the number of entries in the traceOverrides array.
     */
    constructor( plotOutputId, htmlElementIds, traces, layoutOptions, configOptions = {} )
    {
        WicaUtils.assert( htmlElementIds.length === ( traces.length * 2 ),
            `There were an incorrect number of data sources (${htmlElementIds.length}) for the number of traces (${traces.length})` );

        this.plotOutputId = plotOutputId;
        this.htmlElementIds = htmlElementIds;

        // Create a single plot data trace array which will be shared for all traces.
        this.plotTracesObj = [];

        for ( let traceIndex = 0; traceIndex < Object.keys( traces ).length; traceIndex++ )
        {
            this.plotTracesObj[ traceIndex ] = traces[ traceIndex ];
            this.plotTracesObj[ traceIndex ].x = [];
            this.plotTracesObj[ traceIndex ].y = [];
        }

        this.plotLayoutObj = layoutOptions;

        // The Plotly annotations feature is used to display messages to the user about,
        // for example, Wica Stream status and/or EPICS channel connection status.
        this.plotLayoutObj.annotations = [ {
            bgcolor: "rgb( 255, 255, 255, 255)", // show against an opaque white background
            font: {size: 12},
            showarrow: false,
            visible: true,
            xref: "paper", yref: "paper", x: 0.5, y: 0.5,
            text: ""
        } ];

        this.plotConfigObj = configOptions;
    }

    /**
     * Activates the plot. That's to stay starts rendering the associated plot object
     * based on information obtained from the configured wica html elements.
     *
     * @implNote
     *
     * Design Goal 1 is to be able to plot datasource of 100Hz with no loss of data.
     * With the default refresh rate of 1 second this means that 100 values may
     * need to be processed per second.
     *
     * Design Goal 2 is to visualise the data from HIPA when SINQ is running.  If the
     * goal is to visualise the data for ten hours with pulses every 5 mins this means
     * there are 120 pulses to visualise. If the sampling were to be spaced evenly this
     * means 1 sample every 30 seconds
     *
     * @param {Number} refreshRateInMilliseconds- The rate at which the plot should update.
     *
     */
    activate( refreshRateInMilliseconds = 1000)
    {
        // Set up a Wica PlotBuffer to observe the wica data streamed to the
        // specified wica-aware data elements. Activate the buffer so that each
        // mutation of the 'wica-channel-metadata' and 'wica-channel-value-array'
        // attributes are captured in the buffer. The buffer is only one sample
        // long, meaning that data will be thrown away if the rate of updating
        // the plot is slower than the rate of obtaining new samples.
        this.dataSource = new PlotBuffer( this.htmlElementIds, 1 );
        this.dataSource.activate();
        this.startPlotCycle( refreshRateInMilliseconds );
    }

    startPlotCycle( refreshRateInMilliseconds )
    {
        try
        {
            this.updatePlot();
        }
        catch( err )
        {
            XyPlotDirect.logExceptionData_("Programming Error: updatePlot threw an exception. ", err );
        }

        // Reschedule next update
        setTimeout( () => this.startPlotCycle( refreshRateInMilliseconds ), refreshRateInMilliseconds );
    }

    updatePlot()
    {
        if ( ! this.dataSource.isConnectedToServer()) {
            this.plotlyShowErrorMessage("Wica Server Not Connected");
            this.plotlyRepaint();
            return;
        }

        if ( ! this.dataSource.isDataAvailable()) {
            this.plotlyShowErrorMessage("EPICS Channel(s) Not Connected");
            this.plotlyRepaint();
            return;
        }

        // Goal: get the latest information from the data source and plot it.
        const streamValues = this.dataSource.getValueMap();
        for (const channelValues of Object.values( streamValues) )
        {
            if ( ! WicaUtils.checkRemoteDataSourceOnline(...channelValues ) )
            {
                this.plotlyShowErrorMessage("EPICS Channel(s) Offline");
                this.plotlyRepaint();
                return;
            }
        }

        // Start with a blank canvas (ie no data points). Transfer all data points that are
        // saved in the datasource. If the data source is of a scalar type then convert it
        // to a single valued array (means the plot will contain a single point).
        for ( let traceIndex = 0; traceIndex * 2 < Object.keys( streamValues ).length; traceIndex++ )
        {
            const channelNameX = Object.keys( streamValues )[ 2 * traceIndex ];
            const channelNameY = Object.keys( streamValues )[ 2 * traceIndex + 1 ];
            const channelValueX = XyPlotDirect.forceTypeToArray( streamValues[ channelNameX ][ 0 ].val );
            const channelValueY = XyPlotDirect.forceTypeToArray( streamValues[ channelNameY ][ 0 ].val );

            this.plotTracesObj[ traceIndex ].x = channelValueX;
            this.plotTracesObj[ traceIndex ].y = channelValueY;
        }

        // Turn off any error messages which may be present and repaint the plot.
        this.plotlyClearErrorMessage();
        this.plotlyRepaint();
    }

    plotlyShowErrorMessage( errorMessage )
    {
        this.plotLayoutObj.annotations[ 0 ].text = errorMessage;
        this.plotLayoutObj.annotations[ 0 ].visible = true;
    }

    plotlyClearErrorMessage()
    {
        this.plotLayoutObj.annotations[ 0 ].visible = false;
    }

    plotlyRepaint()
    {
        // It is important to always update the layout data revision so that
        // Plotly knows it must perform a redraw operation.
        const timeNowInMillisSincePosixEpochStart = new Date();
        this.plotLayoutObj.datarevision= timeNowInMillisSincePosixEpochStart;

        const fixedRange = false;
        if ( fixedRange )
        {
            const timeTenHoursAgoInMillisSincePoxixEpochStart = timeNowInMillisSincePosixEpochStart - (1000 * 60 * 60 * 10);
            this.plotLayoutObj.xaxis.range = [ timeTenHoursAgoInMillisSincePoxixEpochStart, timeNowInMillisSincePosixEpochStart ];
        }
        const plotTracesArray = Object.values( this.plotTracesObj );

        // Now repaint the element
        Plotly.react( this.plotOutputId, plotTracesArray, this.plotLayoutObj, this.plotConfigObj );
    }

    static forceTypeToArray( value )
    {
        return Array.isArray( value ) ? value: [ value ];
    }

    /**
     * Log any error data generated in this class.
     *
     * @private
     * @param {string} msg - custom error message.
     * @param {Error} err - the Error object
     */
    static logExceptionData_( msg, err )
    {
        let vDebug = "";
        for ( const prop in err )
        {
            if ( Object.hasOwnProperty.call( err, prop ) )
            {
                vDebug += "property: " + prop + " value: [" + err[ prop ] + "]\n";
            }
        }
        vDebug += "Details: [" + err.toString() + "]";
        console.warn( msg + vDebug );
    }

}
