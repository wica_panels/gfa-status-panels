console.debug( "Executing script in xy-plot-config.js module...");

export function xy_plot_config()
{
    // Configure the Plot Options.
    // Define the buttons that are not required in the modebar. The idea is to leave just
    // the buttons required for minimal functionality: "zoom2d", "pan2d" and axis reset.
    let unneededModeBarButtons = ["toImage", "sendDataToCloud", "toggleSpikelines", "select2d", "lasso2d",
        "zoomIn2d", "zoomOut2d",
        "autoScale2d", "hoverClosestCartesian", "hoverCompareCartesian"];

    const config = {
        displayModeBar: false,
        staticPlot: true,   // by default the plot will be static unless the media queries show some
                            // improved user experience is possible.
        responsive: true,   // set responsive design so that plot resizes if user changes the window dimensions.
        displaylogo: false, // remove Plotly icon from the modebar.
        modeBarButtonsToRemove: unneededModeBarButtons,
    }

    // If the media device supports hover add the possibility of automatically showing
    // the modebar when hovering.
    if (window.matchMedia("(hover:hover)").matches) {
        // Remove override, allowing the normal default Plotly behaviour: modebar appears
        // when hovering over the plot.
        delete config.displayModeBar;
    }

    // If the media device supports any kind of pointer (coarse or fine) then enable
    // the plot's dynamic capabilities. On a touch devices this can be used to support
    // zooming.
    if (!window.matchMedia("(any-pointer:none)").matches) {
        config.staticPlot = false;
    }
    return config;
}