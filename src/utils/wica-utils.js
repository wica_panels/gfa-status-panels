console.debug( "Executing script in wica-utils.js module...");

export default {
    checkStreamStateOpened,
    checkStreamStateConnecting,
    checkStreamStateClosed,
    checkChannelStateConnected,
    checkChannelStateDisconnected,
    getStreamStateAttemptNumber,
    checkRemoteDataSourceOnline,
    assert,
    objectKeysPresentInTarget
};


/**
 * Checks the value of the 'data-wica-stream-state' attribute for each of the supplied wica-aware
 * HTML elements to determine whether the connection to the wica server is in the "opened" state.
 *
 * @param {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result set true when all elements are in the "opened" state.
 */
function checkStreamStateOpened( ...wicaHtmlElements )
{
    return checkStreamState( "opened", ...wicaHtmlElements );
}

/**
 * Checks the value of the 'data-wica-stream-state' attribute for each of the supplied wica-aware
 * HTML elements to determine whether the connection to the wica server is in the "connecting" state.
 *
 * @param {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result set true when all elements are in the "connecting" state.
 */
function checkStreamStateConnecting( ...wicaHtmlElements )
{
    return checkStreamState( "connect", ...wicaHtmlElements );
}

/**
 * Checks the value of the 'data-wica-stream-state' attribute for each of the supplied wica-aware
 * HTML elements to determine whether the connection to the wica server is in the "closed" state.
 *
 * @param  {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result set true when all elements are in the "closed" state.
 */
function checkStreamStateClosed( ...wicaHtmlElements )
{
    return checkStreamState( "closed", ...wicaHtmlElements );
}

/**
 * Attempts to extract the attempt number associated with the stream state attribute
 * of the supplied wica element.
 *
 * Example: for connection attempt number 123 the 'data-wica-stream-state' element
 * is expected to be in one of the following states: 'connect-123', 'opened-123',
 * 'closed-123'.
 *
 * @param  {HTMLElement} wicaHtmlElement - The HTML element to be used as
 *    indicative of the current wica stream connection state.
 * @returns {string} the result, or "xxx" if the attribute is unavailable
 *    or is not formatted as expected.
 */
function getStreamStateAttemptNumber( wicaHtmlElement )
{
    if ( ! wicaHtmlElement.hasAttribute( "data-wica-stream-state" ) ) {
        return "xxx";
    }

    const streamState = wicaHtmlElement.getAttribute("data-wica-stream-state" );
    const result = streamState.split( "-", 2 );

    if ( result.length !== 2 )
    {
        return "xxx";
    }
    return result[ 1 ];
}

/**
 * Checks the value of the 'data-wica-stream-state' attribute for each of the supplied wica-aware
 * HTML elements to determine whether the connection to the wica server is in the expected state.
 *
 * @param {string} expectedState - string specifying the expected state.
 * @param  {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result set true when all elements are in the expected state.
 */
function checkStreamState( expectedState, ...wicaHtmlElements )
{
    for( const ele of wicaHtmlElements ) {
        if ( ! ele.hasAttribute( "data-wica-stream-state" ) ) {
            return false;
        }
        if ( ! ele.getAttribute( "data-wica-stream-state" ).startsWith( expectedState ) ) {
            return false;
        }
    }
    return true;
}

/**
 * Checks the value of the 'data-wica-channel-connection-state' attribute for each of the
 * supplied wica-aware HTML elements to determine whether the connection between the wica
 * server and the underlying data channel is in the "connected" state.
 *
 * @param  {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} - the result set true when all elements are in the "connected" state.
 */
function checkChannelStateConnected( ...wicaHtmlElements )
{
    return checkChannelState( "connected", ...wicaHtmlElements );
}

/**
 * Checks the value of the 'data-wica-channel-connection-state' attribute for each of the
 * supplied wica-aware HTML elements to determine whether the connection between the wica server
 * and the underlying data channel is in the "disconnected" state.
 *
 * @param {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result set true when all elements are in the "disconnected" state.
 */
function checkChannelStateDisconnected( ...wicaHtmlElements )
{
    return checkChannelState( "disconnected", ...wicaHtmlElements );
}

/**
 * Checks the value of the 'data-wica-channel-connection-state' attribute for each of the
 * supplied wica-aware HTML elements to determine whether the connection between the wica server
 * and the underlying data channel is in the expected state.
 *
 * @param {string} expectedState - The expected state.
 * @param {...Object} wicaHtmlElements - The elements to check.
 * @returns {boolean} the result, set true when all elements are in the expected state.
 */
function checkChannelState( expectedState, ...wicaHtmlElements )
{
    for( const ele of  wicaHtmlElements ) {
        if ( ! ele.hasAttribute( "data-wica-channel-connection-state" ) ) {
            return false;
        }
        if ( ! ele.getAttribute( "data-wica-channel-connection-state" ).startsWith( expectedState ) ) {
            return false;
        }
    }
    return true;
}

/**
 * Checks that all the arguments in the supplied list of wica channel value objects
 * indicate that they have valid information from the remote data source (ie that
 * they do not indicate that the channel is currently offline)
 *
 * @param {...Object} wicaChannelValueObject
 * @returns {boolean} the result, set true when ALL objects have valid remote data
 */

function checkRemoteDataSourceOnline( ...wicaChannelValueObject )
{
    for ( const obj of wicaChannelValueObject )
    {
        // Currently wica indicates that a channel is offline by setting the
        // object to null, This may change in a future release.
        if ( obj === null ) {
            return false;
        }
    }
    return true;
}

/**
 * Logs an error message to the console if the supplied assertion condition is not met.
 *
 * @param {boolean} condition - The condition to check.
 * @param {string} errorMessage - The error message.
 */
function assert( condition, errorMessage ) {
    if (! condition ) {
        console.error( errorMessage );
    }
}

/**
 * Returns a boolean indication whether all the properties in the source object
 * are present in the target object.
 *
 * @param srcObject
 * @param targetObject
 * @returns {boolean} the result, set true if all keys were found.
 */
function objectKeysPresentInTarget( srcObject, targetObject )
{
    const srcKeys = Object.keys( srcObject );
    const targetKeys = Object.keys( targetObject );

    return srcKeys.every( (key) => {
        return targetKeys.indexOf(key) >= 0;
    } );
}
