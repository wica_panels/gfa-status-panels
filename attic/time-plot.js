console.debug( "Executing script in time-plot.js module...");

import Plotly from "plotly.js-basic-dist";
import {PlotBuffer} from "@psi/wica-js/client-api.js";
import WicaUtils from "../utils/wica-utils.js";
import TimePlotTemplate from "./time-plot-template.js";

export class TimePlot
{
    /**
     * Creates a new plot for visualizing time series data.
     *
     * @param {Object} plotOutputId - The HTML element where the plot is to be rendered.
     *
     * @param {String[]} htmlElementIds - The elements providing the datasources for this plot.
     *
     * @param {Object[]} traceOverrides - An array with each entry providing a set of
     *        trace override arguments for a trace within the plot.
     *
     * @param {Object} layoutOverrides - Provides the layout override arguments.
     *
     * @param {Object} configOverrides - An optional parameter providing the configuration
     *        override arguments.
     *
     * Note: the number of elements in the htmlElementIds array should exactly match the number
     *       of traces in the plot and the number of entries in the traceOverrides array.
     */
    constructor( plotOutputId, htmlElementIds, traceOverrides, layoutOverrides, configOverrides = {} )
    {
        WicaUtils.assert( htmlElementIds.length === traceOverrides.length,
                         `There was a mismatch between the size of the 'htmlElementIds' array (${htmlElementIds.length}) and that of the 'traceOverrides' array (${traceOverrides.length})` );

        this.plotOutputId = plotOutputId;
        this.htmlElementIds = htmlElementIds;

        // Create a single plot data trace array which will be shared for all traces.
        this.plotTracesObj = [];

        for ( let traceIndex = 0; traceIndex < Object.keys( traceOverrides ).length; traceIndex++ )
        {
            const overrides = traceOverrides[ traceIndex ];
            const channelName = document.getElementById( htmlElementIds[ traceIndex ]).getAttribute( "data-wica-channel-name" );
            this.plotTracesObj[ channelName ] = ( TimePlotTemplate.getTrace( overrides ) );
            this.plotTracesObj[ channelName ].x = [];
            this.plotTracesObj[ channelName ].y = [];
        }

        this.plotLayoutObj = TimePlotTemplate.getLayout( layoutOverrides );
        this.plotConfigObj = TimePlotTemplate.getConfig( configOverrides );
    }

    /**
     * Activates the plot. That's to stay starts rendering the associated plot object
     * based on information obtained from the configured wica html elements.
     *
     * @implNote
     *
     * Design Goal 1 is to be able to plot datasource of 100Hz with no loss of data.
     * With the default refresh rate of 1 second this means that 100 values may
     * need to be processed per second.
     *
     * Design Goal 2 is to visualise the data from HIPA when SINQ is running.  If the
     * goal is to visualise the data for ten hours with pulses every 5 mins this means
     * there are 120 pulses to visualise. If the sampling were to be spaced evenly this
     * means 1 sample every 30 seconds
     *
     * @param {Number} [refreshRateInMilliseconds = 1000] - The rate at which the plot should update.
     *
     * @param {Number} [maximumNumberOfSavedDataPoints = 1000] - The maximum number of points to be
     *                 saved within the plot. Once this limit has been reached the arrival of fresh
     *                 data will cause the oldest trace point will be discarded.
     *
     * @param {Number} [maximumPointAgeInSeconds = 3600] - the timestamp of the oldest point that will
     *                 be included in the plot relative to the current time.
     *
     */
    activate( refreshRateInMilliseconds = 1000, maximumNumberOfSavedDataPoints = 1000, maximumPointAgeInSeconds = 3600 )
    {
        this.dataSource = new PlotBuffer( this.htmlElementIds, maximumNumberOfSavedDataPoints );
        this.dataSource.activate();
        this.maximumPointAgeInSeconds = maximumPointAgeInSeconds;

        this.startPlotCycle( refreshRateInMilliseconds );
    }

    startPlotCycle( refreshRateInMilliseconds )
    {
        try
        {
            this.updatePlot();
        }
        catch( err )
        {
            TimePlot.logExceptionData_("Programming Error: updatePlot threw an exception. ", err );
        }

        // Reschedule next update
        setTimeout( () => this.startPlotCycle( refreshRateInMilliseconds ), refreshRateInMilliseconds );
    }

    updatePlot()
    {
        if ( ! this.dataSource.isConnectedToServer() ) {
            this.plotlyShowErrorMessage("Wica Server Not Connected");
            this.plotlyRepaint();
            return;
        }

        if ( ! this.dataSource.isDataAvailable() ) {
            this.plotlyShowErrorMessage("EPICS Channel(s) Not Connected");
            this.plotlyRepaint();
            return;
        }

        // Get the latest information from the data source and plot it.
        const channelValueMap = this.dataSource.getValueMap();
        for ( const channelValues of Object.values( channelValueMap) )
        {
            if ( ! WicaUtils.checkRemoteDataSourceOnline(...channelValues ) )
            {
                this.plotlyShowErrorMessage("EPICS Channel(s) Offline");
                this.plotlyRepaint();
                return;
            }
        }

        // Start with a blank canvas (ie no data points). Transfer all data points that are saved in the datasource.
        for ( const channelName of Object.keys( channelValueMap ) )
        {
            this.plotTracesObj[ channelName ].x =[];
            this.plotTracesObj[ channelName ].y =[];

            const channelValues = channelValueMap[ channelName ];
            for ( const channelValue of channelValues )
            {
                const tsDatePoint = new Date( channelValue.ts );
                const tsDateNow = new Date();
                if ( TimePlot.timeDifferenceInSeconds( tsDateNow, tsDatePoint ) < this.maximumPointAgeInSeconds )
                {
                    this.plotTracesObj[channelName].x.push(channelValue.ts);
                    this.plotTracesObj[channelName].y.push(channelValue.val);
                }
            }
        }

        // Turn off any error messages which may be present and repaint the plot.
        this.plotlyClearErrorMessage();
        this.plotlyRepaint();
    }

    plotlyShowErrorMessage( errorMessage )
    {
        this.plotLayoutObj.annotations[ 0 ].text = errorMessage;
        this.plotLayoutObj.annotations[ 0 ].visible = true;
    }

    plotlyClearErrorMessage()
    {
        this.plotLayoutObj.annotations[ 0 ].visible = false;
    }

    plotlyRepaint()
    {
        // It is important to always update the layout data revision so that
        // Plotly knows it must perform a redraw operation.
        this.plotLayoutObj.datarevision= new Date();

        const plotTracesArray = Object.values( this.plotTracesObj );

        // Now repaint the element
        Plotly.react( this.plotOutputId, plotTracesArray, this.plotLayoutObj, this.plotConfigObj );
    }

    static timeDifferenceInSeconds( ts1, ts2 )
    {
        return ( ts1 - ts2 ) / 1000;
    }

    /**
     * Log any error data generated in this class.
     *
     * @private
     * @param {string} msg - custom error message.
     * @param {Error} err - the Error object
     */
    static logExceptionData_( msg, err )
    {
        let vDebug = "";
        for ( const prop in err )
        {
            if ( Object.hasOwnProperty.call( err, prop ) )
            {
                vDebug += "property: " + prop + " value: [" + err[ prop ] + "]\n";
            }
        }
        vDebug += "Details: [" + err.toString() + "]";
        console.warn( msg + vDebug );
    }
}
