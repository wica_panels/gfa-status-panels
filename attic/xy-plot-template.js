console.debug( "Executing script in xy-plot-template.js module...");

export default { getLayout, getTrace, getConfig };

import WicaUtils from "../utils/wica-utils.js";

/**
 * The default settings for the layout.
 */
const layoutDefaults = { title:            "",
                         titlefont_size:   16,
                         titlefont_color:  "darkblue",
                         margin:           { l: 40, r: 10, b: 60, t: 30, pad: 0 },
                         showlegend:       true,
                         dragmode:         false,
                         legend:           { x: 0, y: 1.2 },
                         xaxis_title:  "",
                         xaxis_range:  [], // Unless an override property is specified the X-axis will be set to autorange.
                         xaxis_tickformat:  "",
                         xaxis_nticks:  "",
                         xaxis_tick0:  "",
                         xaxis_dtick:  "",
                         xaxis_tickwidth:  "",
                         xaxis_ticklen:  "",
                         yaxis_title:  "", yaxis_range:  [], yaxis_tickformat:  "", yaxis_nticks:  "", yaxis_tick0:  "", yaxis_dtick:  "", yaxis_tickwidth:  "", yaxis_ticklen:  "", yaxis_type: "linear", yaxis_side: "left",
                         yaxis2_title: "", yaxis2_range: [], yaxis2_tickformat: "", yaxis2_nticks: "", yaxis2_tick0: "", yaxis2_dtick: "", yaxis2_tickwidth: "", yaxis2_ticklen: "", yaxis2_type: "linear", yaxis2_side: "left",
                         yaxis3_title: "", yaxis3_range: [], yaxis3_tickformat: "", yaxis3_nticks: "", yaxis3_tick0: "", yaxis3_dtick: "", yaxis3_tickwidth: "", yaxis3_ticklen: "", yaxis3_type: "linear", yaxis3_side: "left",
                         yaxis4_title: "", yaxis4_range: [], yaxis4_tickformat: "", yaxis4_nticks: "", yaxis4_tick0: "", yaxis4_dtick: "", yaxis4_tickwidth: "", yaxis4_ticklen: "", yaxis4_type: "linear", yaxis4_side: "left",
                         annotations_text: "Waiting to Connect..."
};

/**
 * Returns a Plotly-compatible plot layout object based on the supplied layout overrides and the default settings.
 *
 * @param {Object} layoutOverrides an object with properties which can override the default layout settings.
 * @returns {Object} the Plotly compatible layout object.
 *
 */
function getLayout( layoutOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( layoutOverrides, layoutDefaults ), `One or more of the following layout properties were unrecognised: (${Object.keys( layoutOverrides )}).` );

    const layoutSettings = Object.assign( {}, layoutDefaults, layoutOverrides );

    const autorangeX = ( layoutSettings.xaxis_range.length === 0 );
    const autorangeY1 = ( layoutSettings.yaxis_range.length === 0 );
    const autorangeY2 = ( layoutSettings.yaxis2_range.length === 0 );
    const autorangeY3 = ( layoutSettings.yaxis3_range.length === 0 );
    const autorangeY4 = ( layoutSettings.yaxis4_range.length === 0 );
    const show_annot = ( layoutSettings.annotations_text.length > 0 );
    const showticklabels1 = ( layoutSettings.yaxis_title.length  >  0 );
    const showticklabels2 = ( layoutSettings.yaxis2_title.length >  0 );
    const showticklabels3 = ( layoutSettings.yaxis3_title.length >  0 );
    const showticklabels4 = ( layoutSettings.yaxis4_title.length >  0 );

    // Define the layout object which will be passed to the Plotly library. The layout properties
    // below are always explicitly set. The property values can be either hardcoded, or taken from
    // the layoutSettings object which is built from the template defaults with optional overrides
    // set by the user.
    const layout =  {
        autosize: true,
        title: layoutSettings.title,
        titlefont: { size: layoutSettings.titlefont_size, color: layoutSettings.titlefont_color },
        margin: layoutSettings.margin,
        showlegend: layoutSettings.showlegend,
        dragmode: layoutSettings.dragmode,
        legend: layoutSettings.legend,
        xaxis:  { title: layoutSettings.xaxis_title, range: layoutSettings.xaxis_range, autorange: autorangeX },
        yaxis:  { title: layoutSettings.yaxis_title,  type: layoutSettings.yaxis_type,  range: layoutSettings.yaxis_range,  autorange: autorangeY1, side: layoutSettings.yaxis_side,  showgrid: true, showticklabels: showticklabels1 },
        yaxis2: { title: layoutSettings.yaxis2_title, type: layoutSettings.yaxis2_type, range: layoutSettings.yaxis2_range, autorange: autorangeY2, side: layoutSettings.yaxis2_side, showgrid: false, overlaying: "y", showticklabels: showticklabels2 },
        yaxis3: { title: layoutSettings.yaxis3_title, type: layoutSettings.yaxis3_type, range: layoutSettings.yaxis3_range, autorange: autorangeY3, side: layoutSettings.yaxis3_side, showgrid: false, overlaying: "y", showticklabels: showticklabels3 },
        yaxis4: { title: layoutSettings.yaxis4_title, type: layoutSettings.yaxis4_type, range: layoutSettings.yaxis4_range, autorange: autorangeY4, side: layoutSettings.yaxis4_side, showgrid: false, overlaying: "y", showticklabels: showticklabels4 },
        annotations: [ {
            bgcolor: "rgb( 255, 255, 255, 255)",
            font: {size: 12},
            showarrow: false,
            visible: show_annot,
            xref: "paper", yref: "paper", x: 0.5, y: 0.5, text: layoutSettings.annotations_text
        } ],
        paper_bgcolor: "rgba(0,0,0,0)",
        plot_bgcolor: "rgba(0,0,0,0)",
    };

    // Add layout properties that will only be added if explicitly provided. These properties
    // should be set to "" in the layoutDefaults definition.
    addObjectPropertyWhenAvailable( layout.xaxis, "nticks",     layoutSettings.xaxis_nticks );
    addObjectPropertyWhenAvailable( layout.xaxis, "tick0",      layoutSettings.xaxis_tick0 );
    addObjectPropertyWhenAvailable( layout.xaxis, "dtick",      layoutSettings.xaxis_dtick );
    addObjectPropertyWhenAvailable( layout.xaxis, "tickwidth",  layoutSettings.xaxis_tickwidth );
    addObjectPropertyWhenAvailable( layout.xaxis, "ticklen",    layoutSettings.xaxis_ticklen );
    addObjectPropertyWhenAvailable( layout.xaxis, "tickformat", layoutSettings.xaxis_tickformat );

    addObjectPropertyWhenAvailable( layout.yaxis, "nticks",     layoutSettings.yaxis_nticks );
    addObjectPropertyWhenAvailable( layout.yaxis, "tick0",      layoutSettings.yaxis_tick0 );
    addObjectPropertyWhenAvailable( layout.yaxis, "dtick",      layoutSettings.yaxis_dtick );
    addObjectPropertyWhenAvailable( layout.yaxis, "tickwidth",  layoutSettings.yaxis_tickwidth );
    addObjectPropertyWhenAvailable( layout.yaxis, "ticklen",    layoutSettings.yaxis_ticklen );
    addObjectPropertyWhenAvailable( layout.yaxis, "tickformat", layoutSettings.yaxis_tickformat );

    addObjectPropertyWhenAvailable( layout.yaxis2, "nticks",     layoutSettings.yaxis2_nticks );
    addObjectPropertyWhenAvailable( layout.yaxis2, "tick0",      layoutSettings.yaxis2_tick0 );
    addObjectPropertyWhenAvailable( layout.yaxis2, "dtick",      layoutSettings.yaxis2_dtick );
    addObjectPropertyWhenAvailable( layout.yaxis2, "tickwidth",  layoutSettings.yaxis2_tickwidth );
    addObjectPropertyWhenAvailable( layout.yaxis2, "ticklen",    layoutSettings.yaxis2_ticklen );
    addObjectPropertyWhenAvailable( layout.yaxis2, "tickformat", layoutSettings.yaxis2_tickformat );

    addObjectPropertyWhenAvailable( layout.yaxis3, "nticks",     layoutSettings.yaxis3_nticks );
    addObjectPropertyWhenAvailable( layout.yaxis3, "tick0",      layoutSettings.yaxis3_tick0 );
    addObjectPropertyWhenAvailable( layout.yaxis3, "dtick",      layoutSettings.yaxis3_dtick );
    addObjectPropertyWhenAvailable( layout.yaxis3, "tickwidth",  layoutSettings.yaxis3_tickwidth );
    addObjectPropertyWhenAvailable( layout.yaxis3, "ticklen",    layoutSettings.yaxis3_ticklen );
    addObjectPropertyWhenAvailable( layout.yaxis3, "tickformat", layoutSettings.yaxis3_tickformat );

    addObjectPropertyWhenAvailable( layout.yaxis4, "nticks",     layoutSettings.yaxis4_nticks );
    addObjectPropertyWhenAvailable( layout.yaxis4, "tick0",      layoutSettings.yaxis4_tick0 );
    addObjectPropertyWhenAvailable( layout.yaxis4, "dtick",      layoutSettings.yaxis4_dtick );
    addObjectPropertyWhenAvailable( layout.yaxis4, "tickwidth",  layoutSettings.yaxis4_tickwidth );
    addObjectPropertyWhenAvailable( layout.yaxis4, "ticklen",    layoutSettings.yaxis4_ticklen );
    addObjectPropertyWhenAvailable( layout.yaxis4, "tickformat", layoutSettings.yaxis4_tickformat );
    return layout;
}

/**
 * The default settings for the trace.
 */
const traceDefaults = { name:          "",
                        x:             [0],
                        y:             [0],
                        marker_size:   6,
                        marker_symbol: "circle",
                        line_width:    3,
                        line_color:    "",
                        fill:          "",
                        fill_color:    "",
                        xaxis:         "x",
                        yaxis:         "y",
                        mode:          "lines+markers"
};

/**
 * Returns a Plotly-compatible plot trace object based on the supplied trace overrides and the default settings.
 *
 * @param {Object} traceOverrides an object with properties which can be used to override the default trace settings.
 * @returns {Object} a Plotly compatible layout object.
 *
 */
function getTrace( traceOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( traceOverrides, traceDefaults ), `One or more of the following trace properties were unrecognised: (${Object.keys( traceOverrides )}).` );

    const traceSettings = Object.assign( {}, traceDefaults, traceOverrides );

    const trace =  {
        type: 'scatter',
        name: traceSettings.name,
        x: traceSettings.x,
        y: traceSettings.y,
        mode: traceSettings.mode,
        marker: { size: traceSettings.marker_size, symbol: traceSettings.marker_symbol },
        line: { width: traceSettings.line_width },
    };

    addObjectPropertyWhenAvailable( trace, "fill", traceSettings.fill );
    addObjectPropertyWhenAvailable( trace, "fillcolor", traceSettings.fill_color );
    addObjectPropertyWhenAvailable( trace, "yaxis", traceSettings.yaxis );
    addObjectPropertyWhenAvailable( trace.line, "color", traceSettings.line_color );
    return trace;
}

/**
 * The default settings for the config.
 */
const configDefaults = {
    //displayModeBar: true,
    //staticPlot: true
};

/**
 * Returns a Plotly-compatible plot config object based on the supplied config overrides and the default settings.
 *
 * @param {Object} configOverrides an object with properties which can be used to override the default config settings.
 * @returns {Object} a Plotly compatible config object.
 */
function getConfig( configOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( configOverrides, configDefaults ),  `One or more of the following configuration properties were unrecognised: (${Object.keys( configOverrides )}).`);

    const configSettings = Object.assign( {}, configDefaults, configOverrides );

    // BUG FIX: 2021-06-24: Zoom2D button has been re-enabled as this allows the user to negate
    // the effects of panning.
    let unneededModeBarButtons = [ "toImage", "sendDataToCloud", "toggleSpikelines", "select2d", "lasso2d",
                                   // "zoom2d", "pan2d",
                                   "autoScale2d", "hoverClosestCartesian", "hoverCompareCartesian" ];

    // 2018-11-24: Mode bar is currently disabled because it potentially causes a crash.
    // See the issue here: https://github.com/plotly/plotly.js/issues/2687
    const PLOTLY_ISSUE_2687_IS_FIXED = true;
    if ( ( configSettings.displayModeBar ) && ( PLOTLY_ISSUE_2687_IS_FIXED ) ) {
        return {
            responsive: true,
            displaylogo: false,
            modeBarButtonsToRemove: unneededModeBarButtons,
            //staticPlot: configSettings.staticPlot
        };
    }
    else {
        return {
            responsive: true,
            displaylogo: false,
            modeBarButtonsToRemove: unneededModeBarButtons,
            //displayModeBar: false,
            //staticPlot: configSettings.staticPlot
        };
    }
}

function addObjectPropertyWhenAvailable( object, property, override )
{
    if ( override !== "" ) {
        object[ property ] = override;
    }

}