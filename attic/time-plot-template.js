console.debug( "Executing script in time-plot-template.js module...");

export default { getLayout, getTrace, getConfig };

import WicaUtils from "../utils/wica-utils.js";

/**
 * JS Object that defines the default values for a plot's layout properties.
 *
 *  @property {string} title_text="" - the text of the plot's title.
 *  @property {number} titlefont_size=16 - the size of plot's title in pixels.
 *  @property {color} titlefont_color="darkblue" - the color of the plot's title.
 *  @property {object} margin - the margins around the plotting area and the padding between the plotting area
 *     and the axis lines.
 *  @property {boolean} showlegend="true" - whether the plot will have a legend describing the plot's traces.
 *  @property {string} dragmode="false" - determines the mode of drag interactions: "zoom", "pan", "select"
 *     etc or turned off.
 *  @property {object} legend - sets the position of the legend in normalised coords (-2 to +3).
 *  @property {Array} xaxis_range - the range of the x axis.
 *  @property {string} yaxis_title - the text of the plot's y axis title.
 *  @property {string} yaxis_type - the type of the plot's y axis eg "linear", "log", "date" etc
 *  @property {Array} yaxis_range - the range of the y axis.
 *  @property {string} yaxis_side - the side where the yaxis will be rendered eg "left" or "right".
 *  @property {string} annotations_text - the annotation text (this is used to show the state of the plot's data source.
 */
const layoutDefaults = { title_text:       "",
                         titlefont_size:   16,
                         titlefont_color:  "darkblue",
                         margin:           { l: 80, r: 80, b: 40, t: 40, pad: 0 },
                         showlegend:       true,  // whether the legend is diplayed
                         dragmode:         false,
                         legend:           { x: 0, y: 1.2 },
                         xaxis_range:      [],
                         yaxis_title:      "", yaxis_type:  "linear", yaxis_range:  [], yaxis_side:  "left",
                         yaxis2_title:     "", yaxis2_type: "linear", yaxis2_range: [], yaxis2_side: "left",
                         yaxis3_title:     "", yaxis3_type: "linear", yaxis3_range: [], yaxis3_side: "left",
                         yaxis4_title:     "", yaxis4_type: "linear", yaxis4_range: [], yaxis4_side: "left",
                         annotations_text: "Waiting to Connect..."
};

/**
 * Returns a Plotly-compatible plot layout object based on the supplied layout overrides and the default settings.
 *
 * @param {Object} layoutOverrides an object with properties which can override the default layout settings.
 * @returns {Object} the Plotly compatible layout object.
 * 
 */
function getLayout( layoutOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( layoutOverrides, layoutDefaults ), `One or more of the following properties were unrecognised :(${Object.keys( layoutOverrides )}).` );

    const layoutSettings = Object.assign( {}, layoutDefaults, layoutOverrides );

    const autorangeX = ( layoutSettings.xaxis_range.length === 0 );
    const autorangeY1 = ( layoutSettings.yaxis_range.length === 0 );
    const autorangeY2 = ( layoutSettings.yaxis2_range.length === 0 );
    const autorangeY3 = ( layoutSettings.yaxis3_range.length === 0 );
    const autorangeY4 = ( layoutSettings.yaxis4_range.length === 0 );
    const show_annot =  ( layoutSettings.annotations_text.length > 0 );
    const showticklabels1 = ( layoutSettings.yaxis_title.length  >  0 );
    const showticklabels2 = ( layoutSettings.yaxis2_title.length >  0 );
    const showticklabels3 = ( layoutSettings.yaxis3_title.length >  0 );
    const showticklabels4 = ( layoutSettings.yaxis4_title.length >  0 );

    return  {
        autosize: true,
        title: layoutSettings.title,
        titlefont: { size: layoutSettings.titlefont_size, color: layoutSettings.titlefont_color },
        margin: layoutSettings.margin,
        showlegend: layoutSettings.showlegend,
        dragmode: layoutSettings.dragmode,
        legend: layoutSettings.legend,
        xaxis: { range: layoutSettings.xaxis_range, autorange: autorangeX },
        yaxis:  { title: layoutSettings.yaxis_title,  type: layoutSettings.yaxis_type,  range: layoutSettings.yaxis_range,  autorange: autorangeY1, side: layoutSettings.yaxis_side,  showgrid: true,  showticklabels: showticklabels1 },
        yaxis2: { title: layoutSettings.yaxis2_title, type: layoutSettings.yaxis2_type, range: layoutSettings.yaxis2_range, autorange: autorangeY2, side: layoutSettings.yaxis2_side, showgrid: false, overlaying: "y", showticklabels: showticklabels2 },
        yaxis3: { title: layoutSettings.yaxis3_title, type: layoutSettings.yaxis3_type, range: layoutSettings.yaxis3_range, autorange: autorangeY3, side: layoutSettings.yaxis3_side, showgrid: false, overlaying: "y", showticklabels: showticklabels3 },
        yaxis4: { title: layoutSettings.yaxis4_title, type: layoutSettings.yaxis4_type, range: layoutSettings.yaxis4_range, autorange: autorangeY4, side: layoutSettings.yaxis4_side, showgrid: false, overlaying: "y", showticklabels: showticklabels4 },
        annotations: [{
            bgcolor: "rgb( 255, 255, 255, 255)",
            font: {size: 12},
            showarrow: false,
            visible: show_annot,
            xref: "paper", yref: "paper", x: 0.5, y: 0.5, text: layoutSettings.annotations_text
        }],
        paper_bgcolor: "rgba(0,0,0,0)",
        plot_bgcolor: "rgba(0,0,0,0)"
    };
}

/**
 * The default settings for the trace.
 */
const traceDefaults = { name:          "",
                        x:             [0],
                        y:             [0],
                        marker_size:   4,
                        marker_symbol: "circle",
                        line_width:    3,
                        line_color:    "",
                        fill:          "",
                        fill_color:    "",
                        xaxis:         "x",
                        yaxis:         "y",
};

/**
 * Returns a Plotly-compatible plot trace object based on the supplied trace overrides and the default settings.
 *
 * @param {Object} traceOverrides an object with properties which can be used to override the default trace settings.
 * @returns {Object} a Plotly compatible layout object.
 *
 */
function getTrace( traceOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( traceOverrides, traceDefaults ), `One or more of the following properties were unrecognised :(${Object.keys( traceOverrides )}).` );

    const traceSettings = Object.assign( {}, traceDefaults, traceOverrides );

    const trace =  {
        type: "scatter",
        name: traceSettings.name,
        x: traceSettings.x,
        y: traceSettings.y,
        mode: "lines+markers",
        marker: { size: traceSettings.marker_size, symbol: traceSettings.marker_symbol },
        line: { width: traceSettings.line_width },
        fill: traceSettings.fill,
        xaxis: traceSettings.xaxis,
        yaxis: traceSettings.yaxis,
    };

    if ( traceSettings.line_color !== "" ) {
        trace.line.color = traceSettings.line_color;
    }

    if ( traceSettings.fill_color !== "" ) {
        trace.fillcolor = traceSettings.fill_color;
    }

    return trace;
}

/**
 * The default settings for the config.
 */
const configDefaults = {
    displayModeBar: true,
    staticPlot: true
};

/**
 * Returns a Plotly-compatible plot config object based on the supplied config overrides and the default settings.
 *
 * @param {Object} configOverrides an object with properties which can be used to override the default config settings.
 * @returns {Object} a Plotly compatible config object.
 */
function getConfig( configOverrides )
{
    WicaUtils.assert( WicaUtils.objectKeysPresentInTarget( configOverrides, configDefaults ), `One or more of the following properties were unrecognised :(${Object.keys( configOverrides )}).` );

    const configSettings = Object.assign( {}, configDefaults, configOverrides );

    // BUG FIX: 2021-06-24: Zoom2D button has been re-enabled as this allows the user to negate
    // the effects of panning.
    let unneededModeBarButtons = [ "toImage", "sendDataToCloud", "toggleSpikelines", "select2d", "lasso2d",
                                   // "zoom2d", "pan2d",
                                   "autoScale2d", "hoverClosestCartesian", "hoverCompareCartesian" ];

    // 2018-11-24: Mode bar is currently disabled because it potentially causes a crash.
    // See the issue here: https://github.com/plotly/plotly.js/issues/2687
    const PLOTLY_ISSUE_2687_IS_FIXED = true;
    if ( ( configSettings.displayModeBar ) && ( PLOTLY_ISSUE_2687_IS_FIXED ) ) {
        return {
            responsive: true,
            displaylogo: false,
            modeBarButtonsToRemove: unneededModeBarButtons,
            staticPlot: configSettings.staticPlot
        };
    }
    else {
        return {
            responsive: true,
            displayModeBar: false,
            staticPlot: configSettings.staticPlot
        };
    }
}

